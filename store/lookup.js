import {api} from '~/api'

export const state = () => ({
  
  good        : [],
  bad         : [],
  hobbies     : [],
  occupations : [],
  foods       : [],
  genres      : [],
  topics      : [],
  sports      : []

})

export const mutations = {

  UPDATE_GOOD(state, payload) {
    state.good = payload
  },

  UPDATE_BAD(state, payload) {
    state.bad = payload
  },

  UPDATE_HOBBIES(state, payload) {
    state.hobbies = payload
  },

  UPDATE_OCCUPATIONS(state, payload) {
    state.occupations = payload
  },

  UPDATE_FOODS(state, payload) {
    state.foods = payload
  },

  UPDATE_GENRES(state, payload) {
    state.genres = payload
  },

  UPDATE_TOPICS(state, payload) {
    state.topics = payload
  },

  UPDATE_SPORTS(state, payload) {
    state.sports = payload
  },

    

}

export const actions = {

  async FETCH_GOOD(context){

    const parameters = {tag: 'good'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_GOOD', res.data) 
      
  },

  async FETCH_BAD(context){

    const parameters = {tag: 'bad'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_BAD', res.data) 
      
  },

  async FETCH_HOBBIES(context){

    const parameters = {tag: 'hobby'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_HOBBIES', res.data) 
      
  },

  async FETCH_OCCUPATIONS(context){

    const parameters = {tag: 'occupation'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_OCCUPATIONS', res.data) 
      
  },

  async FETCH_FOODS(context){

    const parameters = {tag: 'food'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_FOODS', res.data) 
      
  },

  async FETCH_GENRES(context){

    const parameters = {tag: 'genre'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_GENRES', res.data) 
      
  },

  async FETCH_TOPICS(context){

    const parameters = {tag: 'topic'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_TOPICS', res.data) 
      
  },

  async FETCH_SPORTS(context){

    const parameters = {tag: 'sport'}

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('lookups', parameters, options)
  
    context.commit('UPDATE_SPORTS', res.data) 
      
  },


}
