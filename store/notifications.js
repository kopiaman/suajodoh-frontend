import {api} from '~/api'

export const state = () => ({
    
   count_unread_all : '',
   count_notification : '',
   count_letter : '',
   count_sent   : '',


   current_type : '',


   notifications            : '',
   notifications_arr        : [],
   notifications_page       : 1,
   notifications_load_more_status : true,

   notification_show : '',


   letters       : '',
   letters_arr   : [],
   letters_arr_index : 0,

})

export const mutations = {
  
  
  UPDATE_count_unread_all(state, payload) {
    state.count_unread_all = payload
  },


  UPDATE_current_type(state, payload) {
    state.current_type = payload
  },


  

  //notifications all
  UPDATE_notifications(state,payload){
        state.notifications = payload
  },
  UPDATE_notifications_arr(state,payload){
      state.notifications_arr = payload
  },
  CLEAR_notifications_arr(state){
      state.notifications_arr = []
  },
  APPEND_notifications_arr(state,payload){

      payload.forEach(function(item){
          state.notifications_arr.push(item)
      })
  },
  INCREMENT_notifications_page(state){
      state.notifications_page++
  },
  RESET_notifications_page(state){
      state.notifications_page = 1
  },


  UPDATE_notification_show(state, payload) {
    state.notification_show = payload
  },

  UPDATE_notifications_load_more_status(state, payload) {
    state.notifications_load_more_status = payload
  },


}

export const actions = {


  async COUNT_UNREAD_ALL(context){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('user_notifications/count_unread', '', options)

    context.commit('UPDATE_count_unread_all', res.data)

    return res
  },

  async FETCH_NOTIFICATIONS_FRESH(context, params){


    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }
    const res = await api.get('user_notifications', params, options)
    
    context.commit('UPDATE_notifications', res.data) 

    // clear any items from arr
    context.commit('CLEAR_notifications_arr')

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_notifications_arr', items)    


     //check load more status
    if(res.data.current_page == res.data.last_page){
      context.commit('UPDATE_notifications_load_more_status', false)
    }
   
      
  },

  async FETCH_NOTIFICATIONS_APPEND(context, params){

    

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('user_notifications', params, options)
    
    context.commit('UPDATE_notifications', res.data) 

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_notifications_arr', items)


    //check load more status
    if(res.data.current_page == res.data.last_page){
      context.commit('UPDATE_notifications_load_more_status', false)
    }
      
  
  },

 
  async ACTION_READ_NOTIFICATION(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`user_notifications/${id}/read`, '', options)
    
    return res

  },


   async FETCH_NOTIFICATION_SHOW(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get(`user_notifications/${id}`, '', options)

    context.commit('UPDATE_notification_show', res.data) 
    
    return res

  },

  async ACTION_REMOVE(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`user_notifications/${id}/remove`, '', options)
    
    return res

  }



  



}
