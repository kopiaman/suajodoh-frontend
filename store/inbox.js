import {api} from '~/api'

export const state = () => ({
  

  inbox: '',
  inbox_arr : []

  
})

export const mutations = {

  


  UPDATE_INBOX(state, payload) {
    state.inbox = payload
  },

  UPDATE_INBOX_ARR(state, payload) {
    state.inbox_arr = payload
  },

}

export const actions = {

  
  async FETCH_INBOX(context, params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const parameters = {
        page       : params.page
    }

    const res = await api.get(`inboxes`, parameters , options)
      
    console.log(res.data)
    
    context.commit('UPDATE_INBOX', res.data)
    context.commit('UPDATE_INBOX_ARR', res.data.data)

    return res.data

  },


  async RETRIEVE_INBOX_ID(context, params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const parameters = {
        candidate_id       : params.candidate_id
    }

    const res = await api.post(`inboxes/retrieve_id`, parameters , options)
      
    return res.data

  },



 








}
