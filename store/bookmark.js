import {api} from '~/api'

export const state = () => ({
    
  candidates : [],
  candidates_arr : [],
  candidates_arr_index : 0

})

export const mutations = {

  UPDATE_CANDIDATES(state, payload) {
    state.candidates = payload
  },


  CLEAR_CANDIDATES_ARR(state){
    state.candidates_arr = []
  },

  CLEAR_CANDIDATES_ARR_INDEX(state){
    state.candidates_arr_index = 0
  },

  INCREMENT_CANDIDATES_ARR_INDEX(state){
    state.candidates_arr_index++
  },

  DECREMENT_CANDIDATES_ARR_INDEX(state){
    state.candidates_arr_index--
  },

  // only append candidate objet
  APPEND_CANDIDATES_ARR(state, payload){

    payload.forEach( (item) => {
      state.candidates_arr.push(item.candidate)
    })

  }

    

}

export const actions = {

  async FETCH_CANDIDATES_FRESH(context, params){

    const parameters = {
      page       : params.page
    }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('bookmarks', parameters, options)
    
    //
    context.commit('UPDATE_CANDIDATES', res.data) 

    // clear any items from arr
    context.commit('CLEAR_CANDIDATES_ARR')

    //reset candidate arr index
    context.commit('CLEAR_CANDIDATES_ARR_INDEX')

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_CANDIDATES_ARR', items)    

    console.log(res.data)
      
  },

  async FETCH_CANDIDATES_APPEND(context, params){

    const parameters = {
      page       : params.page
    }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('bookmarks', parameters, options)
    
    context.commit('UPDATE_CANDIDATES', res.data) 

    console.log(res.data)

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_CANDIDATES_ARR', items)
      
    
        
  },

  async ACTION_ADD_CANDIDATE(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`bookmarks/${id}/add`, '', options)

    console.log(res)
    return res
    
  },

  async ACTION_REMOVE_CANDIDATES(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`bookmarks/${id}/remove`, '', options)

    return res
    
  }
}
