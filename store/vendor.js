import { api } from "~/api/vendor";

export const state = () => ({
  unsplash_image: "",
  unsplash_user: "",
  unsplash_link: ""
});

export const mutations = {
  UPDATE_unsplash_image(state, payload) {
    state.unsplash_image = payload;
  },

  UPDATE_unsplash_user(state, payload) {
    state.unsplash_user = payload;
  },

  UPDATE_unsplash_link(state, payload) {
    state.unsplash_link = payload;
  }
};

export const actions = {
  async FETCH_UNSPLASH_RANDOM(context, params) {
    const options = {
      headers: {
        "Accept-Version": "v1",
        Authorization: `Client-ID 44d80e117cbeb4e19f2da24f39c4f6e1f961d91228152eca7664baeb67535880`
      }
    };

    const vendor_url = "https://api.unsplash.com";

    const res = await api.get(vendor_url, "photos/random", params, options);

    context.commit("UPDATE_unsplash_user", res.data.user.name);

    let utm = "utm_source=suajodoh&utm_medium=referral";
    context.commit(
      "UPDATE_unsplash_link",
      `${res.data.user.links.html}?${utm}`
    );

    //trigger id
    context.dispatch("FETCH_UNSPLASH_TRIGGER", res.data.id);

    const image = res.data.urls.raw + "&w=500&h=700";
    context.commit("UPDATE_unsplash_image", image);

    return image;
  },

  async FETCH_UNSPLASH_TRIGGER(context, id) {
    const options = {
      headers: {
        "Accept-Version": "v1",
        Authorization: `Client-ID 44d80e117cbeb4e19f2da24f39c4f6e1f961d91228152eca7664baeb67535880`
      }
    };

    const vendor_url = "https://api.unsplash.com";

    const res = await api.get(vendor_url, `photos/${id}/download`, {}, options);
  }
};
