import {api} from '~/api'

export const state = () => ({
    
  is_bookmark: false,
  is_bookmark_remove: false,
  is_salam : false,
  is_card_appear : true,

})

export const mutations = {

  UPDATE_IS_BOOKMARK(state,payload){
    state.is_bookmark = payload
  },

  UPDATE_IS_BOOKMARK_REMOVE(state,payload){
    state.is_bookmark_remove = payload
  },

  UPDATE_IS_SALAM(state,payload){
    state.is_salam = payload
  },

  UPDATE_IS_CARD_APPEAR(state,payload){
    state.is_card_appear = payload
  },

  UPDATE_IS_CARD_APPEAR_FALSE(state){
    state.is_card_appear = false
  },


}

export const actions = {

  
  async ACTION_BLOCK_CANDIDATE(context,params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`candidates/${params.id}/add`, {type: 'block', note: params.note}, options)

    console.log(res)
    return res
    
  },

  async ACTION_GIVE_SALAM(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`candidates/${id}/addSalam`, {}, options)

    console.log(res)
    return res
    
  },

  async ACTION_REPORT_CANDIDATE(context, params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post(`candidates/${params.id}/add`, { type : 'report', note : params.note}, options)

    console.log(res)
    return res
    
  },
}
