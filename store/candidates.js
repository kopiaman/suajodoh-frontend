import {api} from '~/api'

export const state = () => ({
    
  candidates : {},
  candidates_arr : [],

  candidates_arr_index : 0,

  candidate: '',
  show_detail: false,
  show_search: false,

  // filters
  filter_state_born : '',
  filter_state_stay : '',
  filter_occupation : '',
  filter_is_me : '',
  filter_age : '',
  

})

export const mutations = {

  UPDATE_CANDIDATES(state, payload) {
    state.candidates = payload
  },

  UPDATE_CANDIDATE(state, payload) {
    state.candidate = payload
  },

  CLEAR_CANDIDATES_ARR(state){
    state.candidates_arr = []
  },

  CLEAR_CANDIDATES_ARR_INDEX(state){
    state.candidates_arr_index = 0
  },

  INCREMENT_CANDIDATES_ARR_INDEX(state){
    state.candidates_arr_index++
  },

  DECREMENT_CANDIDATES_ARR_INDEX(state){
    state.candidates_arr_index--
  },

  APPEND_CANDIDATES_ARR(state, payload){

    payload.forEach( (item) => {
      state.candidates_arr.push(item)
    })

  },

  TOGGLE_SHOW_DETAIL(state,payload){
    state.show_detail =! state.show_detail
  },

  SET_SHOW_DETAIL(state,payload){
    state.show_detail = true
  },

  SET_HIDE_DETAIL(state,payload){
    state.show_detail = false
  },


  TOGGLE_SHOW_SEARCH(state){
    state.show_search =! state.show_search
  },


  // FILTER MUTATION
  FILTER_STATE_BORN(state, payload) {
    state.filter_state_born = payload
  },

  FILTER_STATE_STAY(state, payload) {
    state.filter_state_stay = payload
  },

  FILTER_OCCUPATION(state, payload) {
    state.filter_occupation = payload
  },

  FILTER_IS_ME(state, payload) {
    state.filter_is_me = payload
  }


    
  

}

export const actions = {

  async FETCH_CANDIDATES_FRESH(context, params){

    const parameters = {

      state_born : params.state_born,
      state_stay : params.state_stay,
      is_me      : params.is_me,
      occupation : params.occupation,
      age        : params.age,
      page       : params.page
    }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('candidates', parameters, options)
    
    //
    context.commit('UPDATE_CANDIDATES', res.data) 

    // clear any items from arr
    context.commit('CLEAR_CANDIDATES_ARR')

    //reset candidate arr index
    context.commit('CLEAR_CANDIDATES_ARR_INDEX')

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_CANDIDATES_ARR', items)    

   
      
  },

  async FETCH_CANDIDATES_APPEND(context, params){

    const parameters = {

      state_born : params.state_born,
      state_stay : params.state_stay,
      is_me      : params.is_me,
      occupation : params.occupation,
      age        : params.age,
      page       : params.page
    }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('candidates', parameters, options)
    
    context.commit('UPDATE_CANDIDATES', res.data) 


    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_CANDIDATES_ARR', items)
      
    
        
  },

  //candidate detail
  async FETCH_CANDIDATE(context,id){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get(`candidates/${id}`, '', options)
    
    context.commit('UPDATE_CANDIDATE', res.data) 

  },

  //candidate detail
  async FETCH_CANDIDATE_BY_CODE(context,code){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get(`candidates/${code}/show_by_code`, '', options)
    
    context.commit('UPDATE_CANDIDATE', res.data) 

  }


  


}
