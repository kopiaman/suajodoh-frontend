import {api} from '~/api'

export const state = () => ({
  
  //current candidate for letter
  candidate :'',
  //current inbox id
  inbox_id : '',


  letters: '',
  letters_arr : [],
  letters_arr_index : 0,

  backgrounds : [],
  background_selected : '',
  
})

export const mutations = {

  UPDATE_CANDIDATE(state, payload) {
    state.candidate = payload
  },

  UPDATE_INBOX_ID(state, payload) {
    state.inbox_id = payload
  },


  UPDATE_LETTERS(state, payload) {
    state.letters = payload
  },

  APPEND_LETTERS_ARR(state, payload){

    payload.forEach( (item) => {
      state.letters_arr.push(item)
    })

  },


   //letters array mutation
  CLEAR_LETTERS_ARR(state){
    state.letters_arr = [],
    state.letters_arr_index = 0
  },

  INCREMENT_LETTERS_ARR_INDEX(state){
    state.letters_arr_index++
  },

  DECREMENT_LETTERS_ARR_INDEX(state){
    state.letters_arr_index--
  },

  // background letter
  UPDATE_BACKGROUNDS(state,payload){
    state.backgrounds = payload
  },

  UPDATE_BACKGROUND_SELECTED(state,payload){
    state.background_selected = payload
  }

}

export const actions = {

  

  async FETCH_LETTERS_FRESH(context, params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const parameters = {
        page       : params.page
    }

    const res = await api.get(`inbox_letter/inbox/${params.inbox_id}`, parameters , options)
      
    console.log(res.data)
    
    context.commit('UPDATE_LETTERS', res.data)

   // clear any items from arr
    context.commit('CLEAR_LETTERS_ARR')

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_LETTERS_ARR', items)   

    return res.data

  },


  async FETCH_LETTERS_APPEND(context, params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const parameters = {
        page       : params.page
    }

    const res = await api.get(`inbox_letter/inbox/${params.inbox_id}`, parameters , options)
    
    context.commit('UPDATE_LETTERS', res.data) 

    console.log(res.data)

    //iterate them tio push to array
    let items = res.data.data
    context.commit('APPEND_LETTERS_ARR', items)
      
    
        
  },



  async ACTION_WRITE_LETTER(context,params){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const parameters = {
        message       : params.message,
        letter_background_id    : params.letter_background_id,
        receiver_id   : params.receiver_id
    }

    const res = await api.post(`inbox_letter/inbox/${params.inbox_id}`, parameters , options)

    return res.data
  },




  async ACTION_READ_LETTER(context, id){


    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get(`inbox_letter/${id}/read`, {}, options)
    
    return res
            
  },


  async FETCH_BACKGROUNDS(context){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get(`inbox_letter/backgrounds`, {}, options)
    
    context.commit('UPDATE_BACKGROUNDS', res.data)
    context.commit('UPDATE_BACKGROUND_SELECTED', res.data[0])

    return res.data
            
  },


  async ACTION_LETTER_COUNT_TODAY(context){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get(`inbox_letter/letter_count_today`, {}, options)
   
    return res.data
            
  },










}
