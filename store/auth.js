import {api} from '~/api'

export const state = () => ({
    
  // auth
  email_auth: false,
  social_auth : false,
  api_token : '',


  profile : {}, 


})

export const mutations = {
  
  // authentication
  UPDATE_EMAIL_AUTH(state, payload) {
    state.email_auth = payload
  },
  UPDATE_SOCIAL_AUTH(state, payload) {
    state.social_auth = payload
  },
  UPDATE_API_TOKEN(state, payload) {
    state.api_token = payload
  },

  // profile data once login
  UPDATE_PROFILE(state, payload) {
    state.profile = payload
  },

  //update biodata
  SET_nickname(state,payload){
    state.profile.nickname = payload
  },
  SET_name(state,payload){
    state.profile.name = payload
  },
  SET_gender(state,payload){
    state.profile.gender = payload
  },
  SET_city_born(state,payload){
    state.profile.city_born = payload
  },
  SET_state_born(state,payload){
    state.profile.state_born = payload
  },
  SET_state_stay(state,payload){
    state.profile.state_stay = payload
  },
  SET_city_stay(state,payload){
    state.profile.city_stay = payload
  },
  SET_occupation(state,payload){
    state.profile.occupation = payload
  },
  SET_education(state,payload){
    state.profile.education = payload
  },
  SET_height(state,payload){
    state.profile.height = payload
  },
  SET_birthdate(state,payload){
    state.profile.birthdate = payload
  },
  SET_intro(state,payload){
    state.profile.intro = payload
  },



  //update favourite
  SET_hobby(state,payload){
    state.profile.hobby = payload
  },
  SET_food(state,payload){
    state.profile.food = payload
  },
  SET_genre(state,payload){
    state.profile.genre = payload
  },
  SET_sport(state,payload){
    state.profile.sport = payload
  },
  SET_read(state,payload){
    state.profile.read = payload
  },
  SET_converse(state,payload){
    state.profile.converse = payload
  },  

  //behavior
  SET_is_me(state,payload){
    state.profile.is_me = payload
  },
  SET_is_notme(state,payload){
    state.profile.is_notme = payload
  },
  SET_is_want(state,payload){
    state.profile.is_want = payload
  },
  SET_is_dontwant(state,payload){
    state.profile.is_dontwant = payload
  },  

  //avatar
    SET_avatar(state,payload){
    state.profile.avatar = payload
  },


}

export const actions = {

  /*OPTIONS(context){

      //const token = context.rootState.auth.api_token
    const token = context.state.api_token
    const options = { headers: { 'Authorization': `Bearer ${token}` } }

    return options
  },*/

  async LOGIN(context,params){

    const res = await api.post('auth/login', params)

    context.commit('UPDATE_PROFILE', res.data.user)
    context.commit('UPDATE_API_TOKEN', res.data.api_token) 

    return res
  },

  async LOGIN_WITHOUT_PASSWORD(context,email){

    const res = await api.post(`auth/login_without_password/${email}`)

    context.commit('UPDATE_PROFILE', res.data.user)
    context.commit('UPDATE_API_TOKEN', res.data.api_token) 

    return res
  },

  async CHANGE_PASSWORD(context,params){

    const options = { headers: { 'Authorization': `Bearer ${context.state.api_token}` } }

    const res = await api.post('me/change_password', params, options)

    return res
  },

  async RESET_PASSWORD(context,params){

    const res = await api.post('auth/reset_password', params, {})

    return res
  },

  async TEMPORARY_PASSWORD(context,params){

    const res = await api.post('auth/temporary_password', params, {})

    return res
  },

  async VERIFY_EMAIL(context,params){

    const res = await api.get('auth/verify', params, {})

    return res
  },

  async REGISTER(context,params){

    const res = await api.post('auth/register', params)
    return res
  },

  async LOGOUT(context,params){

    // const options = { headers: { 'Authorization': `Bearer ${context.state.api_token}` } }

    // const res = await api.post('auth/logout', {}, options)

    context.commit('UPDATE_PROFILE', "")
    context.commit('UPDATE_EMAIL_AUTH', false) 
    context.commit('UPDATE_SOCIAL_AUTH', false) 
    context.commit('UPDATE_API_TOKEN', "")

    // return res
  },


  async FETCH_PROFILE(context){

    const options = { headers: { 'Authorization': `Bearer ${context.state.api_token}` } }

    const res = await api.get('me', {}, options)
    
    context.commit('UPDATE_PROFILE', res.data) 
          
  },

  async RESEND_VERIFICATION(context){

    const options = { headers: { 'Authorization': `Bearer ${context.state.api_token}` } }

    const res = await api.post('me/resend_verification', {}, options)
    return res       
  },


  async ACTION_UPDATE_PROFILE(context, params){

    const options = { headers: { 'Authorization': `Bearer ${context.state.api_token}` } }

    const res = await api.post('me/update', params, options)
    
    context.commit('UPDATE_PROFILE', res.data) 
          
  },



  async ACTION_UPDATE_AVATAR(context, avatar){

    const options = { headers: { 'Authorization': `Bearer ${context.state.api_token}` } }

    let formData = new FormData()

    if(avatar instanceof File){
      formData.append('avatar',avatar) 
    }

    const res = await api.post('me/update_avatar', formData, options)
    
    context.commit('SET_avatar', res.data.avatar)
    return res
          
  },


  //check if user registered or not
  async ACTION_CHECK_USER(context, email){

    const res = await api.get(`auth/check_user/${email}`)
    
    return res
          
  },











}
