import {api} from '~/api'

export const state = () => ({
    
    history : [],

    //limit reason
    limit_note : '',

})

export const mutations = {

  UPDATE_HISTORY(state,payload){
    state.history = payload
  },

  UPDATE_LIMIT_NOTE(state,payload){
    state.limit_note = payload
  }
  

}

export const actions = {

  async FETCH_HISTORY(context){

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.get('subscribes', {}, options)
  
    context.commit('UPDATE_HISTORY', res.data) 
      
  },

  async ACTION_CREATE_BILL(context, params){

    const paramaters = { amount : params.amount,  days : params.days }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post('subscribes/create_bill', paramaters, options)
    
    return res
      
  },

  async ACTION_CHECK_BILL(context, bill_id){

    const res = await api.get(`subscribes/check_bill/${bill_id}`, {}, {})
    
    return res
      
  },

  

  async ACTION_STORE(context, params){

    const paramaters = { 
      //billid
      id            : params.id, 
      collection_id : params.collection_id,
      //user id
      reference_1   : params.reference_1,
      //days
      reference_2   : params.reference_2,

      //amount etc
      amount      : params.amount,
      paid_amount : params.paid_amount,
      paid        : params.paid,
      paid_at     : params.paid_at,
      description : params.description
    }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post('subscribes', paramaters, options)
    
    return res
      
  },

  async ACTION_TRIAL(context, params){

    const paramaters = { days : params.days }

    const options = { headers: { 'Authorization': `Bearer ${context.rootState.auth.api_token}` } }

    const res = await api.post('subscribes/trial', paramaters, options)
    
    return res
      
  },



}
