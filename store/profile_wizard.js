import {api} from '~/api'

export const state = () => ({
    
  step1 : true,
  step2 : false,
  step3 : false,
  step4 : false,
  step5 : false,
  step6 : false,



})

export const mutations = {
  
  // authentication
  ACTIVATE_step1(state) {
    state.step1 = true;state.step2 = false;state.step3 = false;state.step4 = false;state.step5 = false;state.step6 = false
  },
  ACTIVATE_step2(state) {
    state.step1 = false;state.step2 = true;state.step3 = false;state.step4 = false;state.step5 = false;state.step6 = false
  },
  ACTIVATE_step3(state, payload) {
    state.step1 = false;state.step2 = false;state.step3 = true;state.step4 = false;state.step5 = false;state.step6 = false
  },
  ACTIVATE_step4(state, payload) {
    state.step1 = false;state.step2 = false;state.step3 = false;state.step4 = true;state.step5 = false;state.step6 = false
  },
  ACTIVATE_step5(state, payload) {
    state.step1 = false;state.step2 = false;state.step3 = false;state.step4 = false;state.step5 = true;state.step6 = false
  },
  ACTIVATE_step6(state, payload) {
    state.step1 = false;state.step2 = false;state.step3 = false;state.step4 = false;state.step5 = false;state.step6 = true
  }


}

export const actions = {



}
