import vuexCache from "vuex-cache"

//cache for ~33 minutes
export const plugins = [vuexCache({ timeout: 2000000 })]

export const state = () => ({
	pageTitle: "",

	burger_menu: false,

	loading: false
})

export const mutations = {
	UPDATE_LOADING(state, payload) {
		state.loading = payload
	},

	TOGGLE_burger_menu(state) {
		state.burger_menu = !state.burger_menu
	}
}
