import axios from 'axios';

//vendor_url example https://api.unsplash.com

export const fullUrlFrom = (vendor_url, endpoint) => {
  const fullUrl = `${vendor_url}/${endpoint}`
  return fullUrl;
};

export const options = {
  headers : {
       'Content-Type' : 'application/json',
       'Accept' : 'application/json'
  }
}


axios.interceptors.response.use((response) => {
    return response;
  }, (error) => {
    // Do something with request error

   if(error.response !== undefined){
      const handleError = validationErrorHandling(error.response.data.error)
      return Promise.reject(handleError)
   }

   return Promise.reject(error) 
    

})


export function validationErrorHandling(error){

        let combineErrors = [];


        //if errors is object
        if( typeof error === 'object'){
             //iterate the errors and...
            for( let key in error){

              //push error message to combineErrors array
              combineErrors.push(error[key][0])
            }
            return combineErrors.join(`\r\n\<br/>`)

        }else{
          //return string error
          return error
        }
       

}

const fetchUrl = (method, vendor_url, endpoint, params = {}, options = {}) => {

  if ( method == 'GET') {
    return axios({
      method,
      params,
      url: fullUrlFrom(vendor_url, endpoint, options),
      headers: { ...(options.headers || {}) }
    });
  }

  return axios({
    method,
    data: params,
    url: fullUrlFrom(vendor_url, endpoint, options),
    headers: { ...(options.headers || {}) }
  });
};



export const api = {
  async get(vendor_url, endpoint, params, options) {
    return fetchUrl('GET', vendor_url, endpoint, params, options);
  },
  async post(vendor_url,endpoint, params, options) {
    return fetchUrl('POST', vendor_url, endpoint, params, options);
  },
  async put(vendor_url,endpoint, params, options) {
    return fetchUrl('PUT', vendor_url, endpoint, params, options);
  },
  async patch(vendor_url,endpoint, params) {
    return fetchUrl('PATCH', vendor_url, endpoint, params);
  },
  async delete(vendor_url,endpoint, params) {
    return fetchUrl('DELETE', vendor_url, endpoint, params);
  },
  async getToken(vendor_url, endpoint, params) {
    return fetchUrl('POST', vendor_url, endpoint, params);
  },
};




