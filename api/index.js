import axios from 'axios';

//get base url for API
//ie http://seekers.test:8888/api/v1
const API_URL = process.env.BASE_URL

export const fullUrlFrom = (endpoint) => {
  const fullUrl = `${API_URL}/${endpoint}`
  return fullUrl;
};

export const options = {
  headers : {
       'Content-Type' : 'application/json',
       'Accept' : 'application/json'
  }
}


axios.interceptors.response.use((response) => {
    return response;
  }, (error) => {
    // Do something with request error

   if(error.response !== undefined){
      const handleError = validationErrorHandling(error.response.data.error)
      return Promise.reject(handleError)
   }

   return Promise.reject(error) 
    

})


export function validationErrorHandling(error){

        let combineErrors = [];


        //if errors is object
        if( typeof error === 'object'){
             //iterate the errors and...
            for( let key in error){

              //push error message to combineErrors array
              combineErrors.push(error[key][0])
            }
            return combineErrors.join(`\r\n\<br/>`)

        }else{
          //return string error
          return error
        }
       

}

const fetchUrl = (method, endpoint, params = {}, options = {}) => {

  if ( method == 'GET') {
    return axios({
      method,
      params,
      url: fullUrlFrom(endpoint, options),
      headers: { ...(options.headers || {}) }
    });
  }

  return axios({
    method,
    data: params,
    url: fullUrlFrom(endpoint, options),
    headers: { ...(options.headers || {}) }
  });
};



export const api = {
  async get(endpoint, params, options) {
    return fetchUrl('GET', endpoint, params, options);
  },
  async post(endpoint, params, options) {
    return fetchUrl('POST', endpoint, params, options);
  },
  async put(endpoint, params, options) {
    return fetchUrl('PUT', endpoint, params, options);
  },
  async patch(endpoint, params) {
    return fetchUrl('PATCH', endpoint, params);
  },
  async delete(endpoint, params) {
    return fetchUrl('DELETE', endpoint, params);
  },
  async getToken(endpoint, params) {
    return fetchUrl('POST', endpoint, params);
  },
};




