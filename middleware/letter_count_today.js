export default function (context) {

  //check current profile id
  let user_id = context.store.state.auth.profile.id

  let subscribed =  context.store.state.auth.profile.subscribe   

  //retrieve inbox_id
  context.store.dispatch('inbox_letters/ACTION_LETTER_COUNT_TODAY', {})
  .then( (count) => {
      
      if(count >= 5 && !subscribed){
          context.store.commit('subscribe/UPDATE_LIMIT_NOTE', 'Maaf, anda telah melepasi had limit harian untuk hantar surat. Naik taraf sekarang!')
          context.redirect('/promo/limit')
      }
      
  })

 

}