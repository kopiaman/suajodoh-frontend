export default function (context) {

  if( context.store.state.auth.api_token && context.store.state.auth.is_profile_completed){

        context.redirect('/candidates')
  }

}