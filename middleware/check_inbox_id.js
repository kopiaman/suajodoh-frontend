export default function (context) {

  //check current profile id
  let user_id = context.store.state.auth.profile.id  

  //check candidate id
  let candidate_id = context.store.state.inbox_letters.candidate.id  

  //retrieve inbox_id
  context.store.dispatch('inbox/RETRIEVE_INBOX_ID', { candidate_id : candidate_id})
  .then( (res) => {
      //update inbox id
      context.store.commit('inbox_letters/UPDATE_INBOX_ID', res.inbox_id)
  })

 

}