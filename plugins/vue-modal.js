import Vue from 'vue'
import VModal from 'vue-js-modal'
//import VModal from 'vue-js-modal/dist/ssr.index'
 
Vue.use(VModal, { dynamic : true, injectModalsContainer: true , 
  dynamicDefaults: {
          height : "auto",
          width  : "400px" ,
          adaptive : true 
  }}) 