export default {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    title: "SuaJodoh.com | Carian Jodoh & Pasangan Serasi 1st di Malaysia ",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "keywords",
        name: "keywords",
        content: `Cari Jodoh Malaysia, Cari Pasangan Serasi, Ingin Berkahwin, 
      Mencari Jodoh, Jodoh Malaysia, Cari Pasangan Cantik, Platform Jodoh, Website Jodoh, Cari Pasangan Soleh Baik`
      },
      {
        hid: "description",
        name: "description",
        content:
          "Kami menyediakan calon-calon yang serasi dengan perwatakan dan sifat anda. Platform carian jodoh pertama di Malaysia mengikut keserasian calon. Semakin tinggi skor, semakin serasi anda berdua"
      },
      { name: "theme-color", content: "#20CDA4" },
      {
        hid: "og-url",
        property: "og:url",
        content: `https://suajodoh.com`
      },
      {
        hid: "og-title",
        property: "og:title",
        content: `SuaJodoh.com | Carian Jodoh & Pasangan Serasi 1st di Malaysia`
      },
      {
        hid: "og-type",
        property: "og:type",
        content: `article`
      },
      {
        hid: "og-description",
        property: "og:description",
        content: `Platform carian jodoh pertama di Malaysia mengikut keserasian calon. Semakin tinggi skor, semakin serasi anda berdua`
      },
      {
        hid: "og-image",
        property: "og:image",
        content: `https://sgp1.digitaloceanspaces.com/omj/meta-suajodoh.jpg`
      }
    ],
    link: [
      {
        rel: "apple-touch-icon",
        sizes: "57x57",
        href: "/favicon/apple-icon-57x57.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "60x60",
        href: "/favicon/apple-icon-60x60.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "72x72",
        href: "/favicon/apple-icon-72x72.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "76x76",
        href: "/favicon/apple-icon-76x76.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "114x114",
        href: "/favicon/apple-icon-114x114.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "120x120",
        href: "/favicon/apple-icon-120x120.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "144x144",
        href: "/favicon/apple-icon-144x144.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "152x152",
        href: "/favicon/apple-icon-152x152.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/favicon/apple-icon-180x180.png"
      },

      {
        rel: "icon",
        type: "image/png",
        sizes: "192x192",
        href: "/favicon/android-icon-192x192.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/favicon/favicon-32x32.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "96x96",
        href: "/favicon/favicon-96x96.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/favicon/favicon-16x16.png"
      },

      { rel: "manifest", href: "/favicon/manifest.json" }
    ]

    // script: [
    //   { src: 'https://cdn.socialprove.com/statics/js/customer.js?uid=6WjRJGXbcwPRjHqtf8YCHw9B0if2',
    //   async: true, defer: true, id : 'socialprove-js' ,'hid': 'socialprove-js'}
    // ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: ["~/css/main.scss"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // { src: '~plugins/vuex-persist', mode: 'client' },
    { src: "~plugins/vue-filters", ssr: false },
    { src: "~plugins/vue-select", ssr: true },
    { src: "~plugins/vue-modal", ssr: false },
    { src: "~plugins/vue-scroll", ssr: false },
    { src: "~plugins/vuelidate", ssr: true },
    { src: "~plugins/vuex-cache", ssr: false },
    { src: "~plugins/vue-facebook", ssr: false }
    // { src: '~plugins/vue-shepard', ssr: false },
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    "@nuxtjs/dotenv",
    "@nuxtjs/bulma",
    ["@nuxtjs/google-tag-manager", { id: "GTM-WF6Z4ZB", layer: "dataLayer" }],
    "@nuxtjs/toast"
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },

  vendor: [
    // 'shepherd.js'
  ],

  toast: {
    position: "bottom-right",
    theme: "bubble",
    closeOnSwipe: "false",
    register: [
      // Register custom toasts
      {
        name: "my-error",
        message: "Oops...Something went wrong",
        options: {
          type: "error"
        }
      }
    ]
  }
};
